import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AccueilPage } from '../accueil/accueil';
import { ArticlesPage } from '../articles/articles';
import { DatesPage } from '../dates/dates';
import { ImagesPage } from '../images/images';
import { ContactPage } from '../contact/contact';
/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
  template: `
   <ion-tabs>
     <ion-tab tabIcon="heart" tabTitle="Articles" [root]="articles"></ion-tab>
     <ion-tab tabIcon="image" tabTitle="Galerie" [root]="images"></ion-tab>
     <ion-tab tabIcon="calendar" tabTitle="Dates" [root]="dates"></ion-tab>
     <ion-tab tabIcon="contact" tabTitle="Informations" [root]="contact"></ion-tab>
   </ion-tabs>`
})
export class TabsPage {
  accueil : any;
  //cours : any;
  articles : any;
  images : any;
  dates : any;
  contact : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.accueil = AccueilPage;
    this.articles = ArticlesPage;
    this.images = ImagesPage;
    this.dates = DatesPage;
    this.contact = ContactPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
