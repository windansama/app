import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ArticlesPage } from '../articles/articles';
import { DatesPage } from '../dates/dates';
import { ImagesPage } from '../images/images';
import { ContactPage } from '../contact/contact';
import { Storage } from '@ionic/storage'
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the AcceuilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-accueil',
  templateUrl: 'accueil.html'
})
export class AccueilPage {

  login: any
  passwd: any
  remember: boolean
  constructor(public navCtrl: NavController, public http: Http, private toastCtrl: ToastController, private alertCtrl: AlertController, public storage: Storage) {
  this.remember=false;
  this.storage.set('remember', this.remember)
  this.storage.get('remember').then((val)=>{
    console.log(val)
    if(val){
      this.navCtrl.push(TabsPage);
    }
  })
  }
  logIn(){
  console.log(this.remember)
  let answer: any;
  this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?connexion&login='+this.login+'&mdp='+this.passwd).map(res => res.json()).subscribe(data => {
    answer = data;
    if(answer.resultat=="OK"){
      let toast = this.toastCtrl.create({
        message: "Bienvenue dans la "+this.login,
        duration: 3000,
        position: 'bot'
      });
      toast.present();
      this.navCtrl.push(TabsPage);
      if(this.remember){
        this.storage.set('id', this.login)
      }
      this.storage.set('remember', this.remember)
    }
    else if(answer.erreur=="Login ou mot de passe incorrect"){
      let alert = this.alertCtrl.create({
        title: "Erreur",
        subTitle: "Nom d'utilisateur ou Mot de Passe incorect",
      buttons: ['Réessayer']
      });
      alert.present();
    }
  })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccueilPage');
  }

}
