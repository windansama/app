import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AccueilPage } from '../accueil/accueil';
/**
 * Generated class for the ImagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-images',
  templateUrl: 'images.html',
})
export class ImagesPage {
  img : any;
  error : any;
  id : any;
  galerie = "galerie0";
  constructor(public navCtrl: NavController, public navParams: NavParams, public http : Http, public storage : Storage) {
    this.storage.get('id').then((val) => {
      this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?login=classe1&mdp=mdp1').map(res => res.json()).subscribe(data => {
      /*for(let i of data.galeries){
        if("classe"+i.classe == val || i.classe == 0){
          this.img = i;
        }
      }*/
      this.img = data.galeries.filter(item => item.classe == 0 || "classe"+item.classe == val);
      this.id = val;
      console.log(this.img)
        if(this.img === undefined){
        this.error = "Aucune galeries pour votre classe";
      }
      //console.log(error)
    });
  });
}

rafraichirListe(refresher){
  this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?login=classe1&mdp=mdp1')
   .map(res => res.json())
   .subscribe(data => {
        this.img = data.galeries.filter(item => item.classe == 0 || "classe"+item.classe == this.id);
        refresher.complete();
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ImagesPage');
  }

}
