import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AccueilPage } from '../accueil/accueil';
/**
 * Generated class for the ArticlesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-articles',
  templateUrl: 'articles.html',
})
export class ArticlesPage {
  search : string = '';
  articles : any[];
  items : any;
  id : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public storage:Storage) {
    this.storage.get('id').then((val) => {
      console.log(val);
    this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?login=classe1&mdp=mdp1').map(res => res.json()).subscribe(data => {
    this.articles = data.articles.filter(item => item.classe == 0 || "classe"+item.classe == val);
    this.id = val;
    console.log(this.articles);
  });
  });
  }

  rafraichirListe(refresher){
    this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?login=classe1&mdp=mdp1')
     .map(res => res.json())
     .subscribe(data => {
          this.articles = data.articles.filter(item => item.classe == 0 || "classe"+item.classe == this.id);
          refresher.complete();
      });
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticlesPage');
  }

  initializeItems(){
  this.items = this.articles;
}

  getItems(ev: any) {
  // Reset items back to all of the items
  this.initializeItems();
  // set val to the value of the searchbar
  let q = ev.target.value;

  // if the value is an empty string don't filter the items
  if (q && q.trim() != '') {
    this.items = this.items.filter((v) => {
      if(v && q){
        return (v.texte.toLowerCase().indexOf(q.toLowerCase()) > -1 ||v.titre.toLowerCase().indexOf(q.toLowerCase()) > -1);
      }
    })
  }
}

}
