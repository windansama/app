import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AccueilPage } from '../accueil/accueil';
/**
 * Generated class for the DatesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dates',
  templateUrl: 'dates.html',
})
export class DatesPage {
  dates : any;
  stock : any;
  error : any;
  id : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http, public storage: Storage) {
    this.storage.get('id').then((val) => {
      this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?login=classe1&mdp=mdp1').map(res => res.json()).subscribe(data => {
      this.dates = data.dates.filter(item => item.classe == 0 || "classe"+item.classe == val);
      this.id = val;
      console.log(this.dates)
        if(this.dates === undefined){
        this.error = "Aucune date importante pour votre classe";
      }
      //console.log(error)
    });
  });
};
rafraichirListe(refresher){
  this.http.get('http://www.sebastien-thon.fr/cours/M4104Cip/projet/index.php?login=classe1&mdp=mdp1')
   .map(res => res.json())
   .subscribe(data => {
        this.dates = data.dates.filter(item => item.classe == 0 || "classe"+item.classe == this.id);
        refresher.complete();
    });
  }

  /*creerEvent(){
    this.calendar.createEvent();
  }*/
  ionViewDidLoad() {
    console.log('ionViewDidLoad DatesPage');
  }

}
