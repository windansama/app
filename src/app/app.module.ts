import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { AccueilPage } from '../pages/accueil/accueil';
import { IonicStorageModule } from '@ionic/storage'
import { TabsPage } from '../pages/tabs/tabs';
import { ArticlesPage } from '../pages/articles/articles';
import { ImagesPage } from '../pages/images/images';
import { DatesPage } from '../pages/dates/dates';
import { ContactPage } from '../pages/contact/contact';

@NgModule({
  declarations: [
    MyApp,
    AccueilPage,
    TabsPage,
    ArticlesPage,
    ImagesPage,
    DatesPage,
    ContactPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
	HttpModule,
	IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccueilPage,
    TabsPage,
    ArticlesPage,
    ImagesPage,
    DatesPage,
    ContactPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
